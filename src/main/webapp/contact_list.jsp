<%@include file="/common/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>${title}</title><!-- (1) -->
    <link rel="stylesheet" type="text/css"
          href="${contextPath}/css/style.css"><!-- (2) -->
    <script src="${contextPath}/js/prototype.js"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="${contextPath}/js/jquery.js"></script>
    <script type="text/javascript" src="${contextPath}/js/contact_form.js">
    </script>
    <script type="text/javascript" src="${contextPath}/js/contact_list.js">
    </script>
</head>
<body>
<table class="borderAll">
    <div id="contact_list">
        </div>
            <s:link beanclass="ua.proba.action.FileUploadActionBean">Upload file</s:link>
            <br/>
            <d:table name="${actionBean.contacts}" id="contact" requestURI=""
                     defaultsort="1">
                <d:column title="sku" property="sku"
                          sortable="true"/>
                <d:column title="Price" property="price"
                          sortable="true"/>
                <d:column title="Qty" property="qty" sortable="true"/>
                <d:column title="Title" property="title" sortable="true"/>
                <d:column title="Item length" property="item_length" sortable="true"/>
                <d:column title="Item width" property="item_width" sortable="true"/>
                <d:column title="Item height" property="item_height" sortable="true"/>
                <d:column title="Item weight" property="item_weight" sortable="true"/>
                <d:column title="UPC" property="upc" sortable="true"/>
                <d:column title="UPS Bool" property="ups_bool" sortable="true"/>

                <d:column title="Action">

                    <s:link beanclass="ua.proba.action.ContactListActionBean"
                            event="edit" onclick="return ajaxLink(this, '#contact_form');">
                        <%--<s:param name="contactId.id" value="${contact.id}"/>--%>
                        <s:param name="contactId.sku" value="${contact.sku}" />
                        Edit
                    </s:link> |
                    <s:link beanclass="ua.proba.action.ContactListActionBean" event="delete"
                            onclick="return ajaxLink(this, '#contact_list');">
                        <s:param name="contactId.id" value="${contact.id}"/>Delete</s:link>

                </d:column>
            </d:table>
            <br/>
            <br/>



    <div id="contact_form"></div>
</table>
</body>
</html>
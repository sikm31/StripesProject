/* contact_form.js */

function submitForm(button) {
    var form = button.form;
    var params = $(form).serializeArray();
    params.push({name: '_eventName', value: button.name});


    /*
     START:form

     $.post(form.action, params, function(data) {
     $('#contact_form').hide();
     $('#contact_table').html(data);
     });
     return false;
     }
     END:form
     */



    var xhr = $.post(form.action, params, function(data) {
        if (xhr.getResponseHeader('X-Stripes-Success')) {
            $('#contact_form').hide();
            $('#contact_list').html(data);


        }
        else {
            $('#contact_form').html(data);

        }
    });
    return false;
}
<%--
  Created by IntelliJ IDEA.
  User: yuri
  Date: 30.06.2016
  Time: 17:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglibs.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>${title}</title><!-- (1) -->
    <link rel="stylesheet" type="text/css"
          href="${contextPath}/css/style.css"><!-- (2) -->
    <script src="${contextPath}/js/prototype.js"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="${contextPath}/js/jquery.js"></script>
    <script type="text/javascript" src="${contextPath}/js/contact_form.js">
    </script>
    <script type="text/javascript" src="${contextPath}/js/contact_list.js">
    </script>
</head>
<body>

<s:form id="form" action="/ContactList.action">
    <div><s:hidden name="contactId.sku"/></div>
<table class="form">
    <%--<tr>--%>
        <%--<td>sku:</td>--%>
        <%--<td><s:text name="contactId.sku" /></td><!-- (2) -->--%>
    <%--</tr>--%>
    <tr>
        <td>Price:</td>
        <td><s:text name="contactId.price"/></td><!-- (2) -->
    </tr>
    <tr>
        <td>Qty:</td>
        <td><s:text name="contactId.qty"/></td>
    </tr>
    <tr>
        <td>Title:</td>
        <td><s:text name="contactId.title"/></td>
    </tr>
    <tr>
        <td>Item length:</td>
        <td><s:text name="contactId.item_length"/></td>
    </tr>
    <tr>
        <td>Item width:</td>
        <td><s:text name="contactId.item_width"/></td>
    </tr>
    <tr>
        <td>Item height:</td>
        <td><s:text name="contactId.item_height"/></td>
    </tr>
    <tr>
        <td>Item weigh:</td>
        <td><s:text name="contactId.item_weight"/></td>
    </tr>
    <tr>
        <td>UPC:</td>
        <td><s:text name="contactId.upc"/></td>
    </tr>
    <tr>
        <td>UPS Bool:</td>
        <td><s:text name="contactId.ups_bool"/></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <s:submit name="save" value="Save" onclick="return submitForm(this);"/><!-- (3) -->
            <s:submit name="cancel" value="Cancel" onclick="$('#contact_form').hide();"/>
        </td>
    </tr>
</table>
</s:form>

</body>
</html>
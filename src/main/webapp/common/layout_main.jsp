<%--
  Created by IntelliJ IDEA.
  User: yuri
  Date: 30.06.2016
  Time: 7:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="taglibs.jsp"%>

<s:layout-definition>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
    <html>
    <head>
        <title>${title}</title><!-- (1) -->
        <link rel="stylesheet" type="text/css"
              href="${contextPath}/css/style.css"><!-- (2) -->
    </head>
    <body>
    <div id="header">
        <span class="title">${title}</span><!-- (3) -->
    </div>
    <div id="body">
        <s:layout-component name="body"/><!-- (4) -->
    </div>
        <%-- END:this --%>

    <!-- View source links just for convenience -->
    <div style="padding-left: 8px">
        Source:
        <s:link beanclass="ua.proba.action.ViewSourceActionBean">
            <s:param name="filename" value="<%=request.getRequestURI()%>"/>
            JSP
        </s:link> |
        <s:link beanclass="ua.proba.action.ViewSourceActionBean">
            <s:param name="filename" value="${actionBean.class.name}"/>
            Action Bean
        </s:link>
    </div>
        <%-- START:this --%>
    </body>
    </html>
</s:layout-definition>
<%-- END:this --%>

<%--
  Created by IntelliJ IDEA.
  User: yuri
  Date: 01.07.2016
  Time: 23:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglibs.jsp"%>
<html>
<head>
    <title>Title</title>
    <script src="${contextPath}/js/prototype.js"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="${contextPath}/js/jquery.js"></script>
    <script type="text/javascript" src="${contextPath}/js/contact_form.js">
    </script>
    <script type="text/javascript" src="${contextPath}/js/contact_list.js">
    </script>
</head>
<body>
${actionBean.message2}
<s:form action="${pageContext.request.contextPath}">
    <s:file name="fileToUpload" />
    <br/><br/>
    <s:submit name="upload" value="upload"/>
</s:form>
<br/>
${actionBean.message}
<br/>
<s:link beanclass="ua.proba.action.ContactListActionBean">Back main</s:link>
</body>
</html>

package ua.proba.action;

import net.sourceforge.stripes.action.*;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.ObjectUtils;
import ua.proba.dao.ContactDao;
import ua.proba.dao.ContactDaoImpl;
import ua.proba.model.Contact;

import java.io.*;
import java.util.*;

/**
 * Created by yuri on 01.07.2016.
 */

public class FileUploadActionBean extends BaseActionBean {

    private FileBean fileToUpload;
    private String message;
    private String message2;
    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = "\",\"";


    Set<Contact> contactsi = new HashSet<Contact>();
    ContactDao contactDao = new ContactDaoImpl();



    private Contact contactId;

    public Contact getContactId() {
        return contactId;
    }

    public void setContactId(Contact contactId) {
        this.contactId = contactId;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    @DefaultHandler
    public Resolution index() {
        return new ForwardResolution("file_to_upload.jsp");

    }

    @HandlesEvent("upload")
    public Resolution upload() throws IOException {
        String saveToLocation = fileToUpload.getFileName();
        fileToUpload.save(new File(saveToLocation));
        message = "File uploaded to " + saveToLocation;
        br = new BufferedReader (new InputStreamReader(new FileInputStream(saveToLocation)));
        //br = new LineNumberReader(new FileReader(saveToLocation));
        while ((line = br.readLine()) != null) {
            try {
                String[] country = line.replaceAll("NULL", "\"NULL\"").split(cvsSplitBy);
                Contact con = new Contact();
                con.setSku(country[0].replaceAll("\"", ""));
                con.setPrice(country[1]);
                con.setQty(country[2]);
                con.setTitle(country[3]);
                con.setItem_length(country[4]);
                con.setItem_weight(country[5]);
                con.setItem_height(country[6]);
                con.setItem_width(country[7]);
                con.setUpc(country[8]);
                con.setUps_bool(country[9].replaceAll("\"", ""));
                contactsi.add(con);
            }catch (ArrayIndexOutOfBoundsException e){
                System.out.println(e);
            }
       }

        Iterator<Contact> itr =contactsi.iterator();
        while(itr.hasNext()) {

            contactDao.save(itr.next());
        }
        fileToUpload.delete();

        return this.index();
    }



    public FileBean getFileToUpload() {
        return fileToUpload;
    }

    public void setFileToUpload(FileBean fileToUpload) {
        this.fileToUpload = fileToUpload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

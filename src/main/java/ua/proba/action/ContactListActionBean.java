package ua.proba.action;

import net.sourceforge.stripes.action.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ua.proba.dao.ContactDao;
import ua.proba.dao.ContactDaoImpl;
import ua.proba.hibernate.util.HibernateUtil;
import ua.proba.model.Contact;

import java.io.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by yuri on 30.06.2016.
 */

public class ContactListActionBean extends BaseActionBean {
    private static final String LIST = "contact_list.jsp";
    private final static String FORM = "contact_form.jsp";
    private final static String DISPLAY = "/ContactList.action?contact=";
    ContactDao contactDao = new ContactDaoImpl();

    private Contact contactId;
    private Contact contact;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Contact getContactId() {
        return contactId;
    }

    public void setContactId(Contact contactId) {
        this.contactId = contactId;
    }

    @DefaultHandler
    public Resolution list() {
        return new ForwardResolution(LIST);
    }

    public Resolution delete() {
          contactDao.delete(contactId.getId());
        return new RedirectResolution(DISPLAY);
    }

    public Resolution edit() {
        contactId = contactDao.getSku(contactId.getSku());
        return new ForwardResolution(FORM);
    }

    public List<Contact> getContacts() {
        return contactDao.read();
    }

    public Resolution save() {
        contactId = contactDao.getSku(contactId.getSku());
        contactDao.edit(contactId);
        //return new RedirectResolution(DISPLAY);
        getContext().getResponse().setHeader("X-Stripes-Success","true");
        return new ForwardResolution(LIST);
    }
//    public Resolution cancel() {
//        return new ForwardResolution(DISPLAY);
//    }

}
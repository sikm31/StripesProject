package ua.proba.action;

import net.sourceforge.stripes.action.*;
import ua.proba.dao.ContactDao;
import ua.proba.dao.ContactDaoImpl;
import ua.proba.model.Contact;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yuri on 30.06.2016.
 */
public abstract class BaseActionBean implements ActionBean {
    private ActionBeanContext actionBeanContext;

    public ActionBeanContext getContext() {
        return actionBeanContext;
    }

    public void setContext(ActionBeanContext actionBeanContext) {
        this.actionBeanContext = actionBeanContext;
    }


}

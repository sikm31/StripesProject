package ua.proba.hibernate.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by yuri on 01.07.2016.
 */
@WebListener
public class HibernateSessionFactoryListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        HibernateUtil.currentSession();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        HibernateUtil.closeSession();
    }

//    @Override
//    public void contextInitialized(ServletContextEvent servletContextEvent) {
//        Configuration configuration = new Configuration();
//        configuration.configure("hibernate.cfg.xml");
//
//        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
//        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
//
//        servletContextEvent.getServletContext().setAttribute("SessionFactory", sessionFactory);
//    }
//
//
//
//    @Override
//    public void contextDestroyed(ServletContextEvent servletContextEvent) {
//        SessionFactory sessionFactory = (SessionFactory) servletContextEvent.getServletContext().getAttribute("SessionFactory");
//        if (sessionFactory != null && !sessionFactory.isClosed()) {
//            sessionFactory.close();
//        }
//    }

}

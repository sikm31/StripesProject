package ua.proba.dao;

import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.MetadataSource;
import org.hibernate.metamodel.MetadataSources;
import ua.proba.hibernate.util.HibernateUtil;
import ua.proba.model.Contact;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yuri on 01.07.2016.
 */
public class ContactDaoImpl implements ContactDao {
    Session session = HibernateUtil.currentSession();


    @Override
    public List<Contact> read() {
        Query query = session.createQuery("from Contact");
        return query.list();
    }

    @Override
    public Contact get(Integer id) {
        Contact contact = (Contact) session.get(Contact.class, id);
        return contact;
    }

    @Override
    public void save(Contact contact) {
        session.save(contact);
       commit();
    }

    @Override
    public void delete(Integer id) {
        Contact contact = (Contact) session.get(Contact.class, id);
        session.delete(contact);
        commit();
    }

    @Override
    public void commit() {
        Transaction tx = session.beginTransaction();
        tx.commit();
    }


    @Override
    public void edit(Contact contact) {
        Contact contact2 = getSku(contact.getSku());
        contact2.setSku(contact.getSku());
        contact2.setPrice(contact.getPrice());
        contact2.setQty(contact.getQty());
        contact2.setTitle(contact.getTitle());
        contact2.setItem_length(contact.getItem_length());
        contact2.setItem_width(contact.getItem_width());
        contact2.setItem_height(contact.getItem_height());
        contact2.setItem_weight(contact.getItem_weight());
        contact2.setUpc(contact.getUpc());
        contact2.setUps_bool(contact.getUps_bool());
        save(contact2);
    }

    @Override
    public Contact getSku(String sku) {
        Criteria criteria = session.createCriteria(Contact.class);
        criteria.add(Restrictions.eq("sku",sku));
        Contact contact = (Contact) criteria.uniqueResult();
        return contact;
    }


}
package ua.proba.dao.mock;

import ua.proba.dao.ContactDao;
import ua.proba.model.Contact;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yuri on 30.06.2016.
 */
public class MockContactDao implements ContactDao {
    private List<Contact> list = new ArrayList<Contact>();
    private Map<String,Contact> map = new HashMap<String,Contact>();
    private int nextId = 1;

    @Override
    public List<Contact> read() {

        return list;
    }

    @Override
    public Contact get(Integer id) {
        return map.get(id);
    }

    @Override
    public void save(Contact contact) {
        list.add(contact);
        map.put(contact.getSku(), contact);

    }

    @Override
    public void delete(Integer id) {
        Contact contact = map.get(id);
        list.remove(contact);
        map.remove(id);
    }

    @Override
    public void commit() {

    }

    @Override
    public void edit(Contact contact) {

    }

    @Override
    public Contact getSku(String sku) {
        return null;
    }

    private static final int SKU   =  0;
    private static final int PRICE   =  1;
    private static final int QTY    =  2;
    private static final int TITLE  =  3;
    private static final int ITEM_LENGTH =  4;
    private static final int ITEM_WIDTH  =  5;
    private static final int ITEM_HEIGHT =  6;
    private static final int ITEM_WEIGHT =  7;
    private static final int ITEM_UPC  =  8;
    private static final int UPC_BOOL  =  9;

    private static final String[] RAW_DATA = {
            "LEX15G642K;6;0;15G642K High-Yield Ink, 15000 Page-Yield, Black;9.8;7.7;19.1;5.6;7,34646E+11;true",
            "XER6R925;8.94;0;6R925 Compatible Remanufactured High-Yield Toner, 10500 Page-Yield, Black;18.56;7.75;11.88;7.94;95205609257;true",
            "XER6R926;5.35;0;6R926 Compatible Remanufactured High-Yield Toner, 11900 Page-Yield, Black;14;10;7.5;4.35;95205609264;true",
            "OKI43990706;14;0;Paper Tray for B400 Series, 530 Sheets;19.5;18.3;9.6;13.23;51851304465;true",
            "PANDQTUT14M;2;0;DQTUT14M Toner, 14,000 Page-Yield, Magenta;19.62;4.12;4.12;2;NULL;true",
            "BRTDS700D;2;0;DSmobile 700D Compact Duplex Scanner, 600 x 600 dpi;4.75;14.87;9.25;1.85;12502630999;true",

    };


    public MockContactDao() {
        try {
            for (String string : RAW_DATA) {
                save(createContactFromString(string));

            }
        }
        catch (Exception exc) {
            throw new RuntimeException(exc);
        }
    }

    private static MockContactDao instance = new MockContactDao();
    public static MockContactDao getInstance() { return instance; }

    public Contact createContactFromString(String string)
            throws Exception
    {
        String[] fields = string.split(";");

        Contact contact = new Contact();
        contact.setSku((fields[SKU]));
        contact.setPrice(fields[PRICE]);
        contact.setQty(fields[QTY]);
        contact.setTitle(fields[TITLE]);
        contact.setItem_length(fields[ITEM_LENGTH]);
        contact.setItem_width(fields[ITEM_WIDTH]);
        contact.setItem_height(fields[ITEM_HEIGHT]);
        contact.setItem_weight(fields[ITEM_WEIGHT]);
        contact.setUpc(fields[ITEM_UPC]);
        contact.setUps_bool(fields[UPC_BOOL]);
        return contact;
    }


}


package ua.proba.dao;

import ua.proba.model.Contact;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yuri on 30.06.2016.
 */
public interface ContactDao {
    List<Contact> read();
    Contact get(Integer id);
    void save(Contact contact);
    void delete(Integer id);
    void commit();
    void edit(Contact contact);
    Contact getSku(String sku);

}

package ua.proba.model;

import javax.persistence.*;

/**
 * Created by yuri on 30.06.2016.
 */
@Entity
public class Contact {

    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private String sku;
    @Column(nullable = true)
    private String price;
    @Column(nullable = true)
    private String qty;
    @Column(nullable = true)
    private String title;
    @Column(nullable = true)
    private String item_length;
    @Column(nullable = true)
    private String item_width;
    @Column(nullable = true)
    private String item_height;
    @Column(nullable = true)
    private String item_weight;
    @Column(nullable = true)
    private String upc;
    @Column(nullable = true)
    private String ups_bool;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getItem_length() {
        return item_length;
    }

    public void setItem_length(String item_length) {
        this.item_length = item_length;
    }

    public String getItem_width() {
        return item_width;
    }

    public void setItem_width(String item_width) {
        this.item_width = item_width;
    }

    public String getItem_height() {
        return item_height;
    }

    public void setItem_height(String item_height) {
        this.item_height = item_height;
    }

    public String getItem_weight() {
        return item_weight;
    }

    public void setItem_weight(String item_weight) {
        this.item_weight = item_weight;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getUps_bool() {
        return ups_bool;
    }

    public void setUps_bool(String ups_bool) {
        this.ups_bool = ups_bool;
    }

    //    @Id
//    @GeneratedValue
//    private int id;
//    private String sku;
//    private Float price;
//    private Integer qty;
//    private String title;
//    private Float item_length;
//    private Float item_width;
//    private Float item_height;
//    private Float item_weight;
//    private String upc;
//    private Boolean ups_bool;
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getSku() {
//        return sku;
//    }
//
//    public void setSku(String sku) {
//        this.sku = sku;
//    }
//
//    public Float getPrice() {
//        return price;
//    }
//
//    public void setPrice(Float price) {
//        this.price = price;
//    }
//
//    public Integer getQty() {
//        return qty;
//    }
//
//    public void setQty(Integer qty) {
//        this.qty = qty;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public Float getItem_length() {
//        return item_length;
//    }
//
//    public void setItem_length(Float item_length) {
//        this.item_length = item_length;
//    }
//
//    public Float getItem_width() {
//        return item_width;
//    }
//
//    public void setItem_width(Float item_width) {
//        this.item_width = item_width;
//    }
//
//    public Float getItem_height() {
//        return item_height;
//    }
//
//    public void setItem_height(Float item_height) {
//        this.item_height = item_height;
//    }
//
//    public Float getItem_weight() {
//        return item_weight;
//    }
//
//    public void setItem_weight(Float item_weight) {
//        this.item_weight = item_weight;
//    }
//
//    public String getUpc() {
//        return upc;
//    }
//
//    public void setUpc(String upc) {
//        this.upc = upc;
//    }
//
//    public Boolean getUps_bool() {
//        return ups_bool;
//    }
//
//    public void setUps_bool(Boolean ups_bool) {
//        this.ups_bool = ups_bool;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        try { return sku.equals(((Contact) obj).getSku()); }
//        catch (Exception exc) { return false; }
//    }
//    @Override
//    public int hashCode() {
//        return 31 + ((sku == null) ? 0 : sku.hashCode());
//    }


}

